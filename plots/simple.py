
import pandas as pd
import matplotlib.pyplot as plt
import os

import pysake.data.measures as meas


# Display curves
# For the four operators each time...

sep = os.environ.get("sep", " ") # default to space


def prepare_data ( cols, ops, nbruns, nbgames, directory, pattern ):
    dataArray = []
    for op in ops:
        df3 = pd.DataFrame(columns = ['rank'])
        df3['rank'] = range( 1, nbgames+1 )
        for col in cols:
            df3[col] = 0
        for i in range( 1, nbruns+1 ):
            df1 = pd.read_csv( directory+pattern.replace( "%%", op ) + "/run"+str(i)+".csv", sep=sep, usecols = cols)
            for col in cols:
                df3[col] += df1[col]
        # Compute average
        for col in cols:
            df3[col] = df3[col]/10
        # Bonus: compute F-measure
        df3['fmeas'] = meas.harmonic_mean( [df3['prec'], df3['rec']] )
        dataArray.append( df3 )
    return dataArray
        
# Different plot_average_curveS (max 4)
linestylelist = ['solid', 'dashed', 'dotted', 'dashdot']

def plot_average_curves( data, labellist, collist, ops, maxvallist, nbruns, nbgames, pattern):
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    labels=''
    for label, col, maxval, ls in zip( labellist, collist, maxvallist, linestylelist ):
        plt.gca().set_prop_cycle(None) # reset color style
        if labels == '':
            title = label
            labels = label
        else:
            title += "/"+label+" ("+ls+")"
            labels += "/"+label
        ind = 0
        for op in ops:
            # Can be changed from 1::100 to 1::10, smoother here (could be nbgames/1000)
            data[ind].iloc[1::100].plot(kind='line',y=col, label=op, ax=ax, linestyle=ls, linewidth=1.0) #, marker='*', color='blue'
            ind += 1
    ax.set_xlabel('games')  # Add an x-label to the axes.
    ax.set_xlim([0,nbgames])
    ax.set_ylabel(labels)  # Add a y-label to the axes.
    ax.set_ylim([0,maxval])
    #ax.set_ylim([0,100])
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    #
    ax.set_title(title+" for "+pattern)  # Add a title to the axes.
    #ax.legend(loc='center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=2)  # Add a legend.
    # reduces legend size to the number of operators...
    lines = ax.get_lines()
    ax.legend( handles=lines[:ind] ) # ncol=2
    #
    plt.show()

def plot_all_measures( directory, pattern, ops ):
    dataframes = prepare_data( ['srate','size','prec','rec'], ops, 10, 20000, directory, pattern )
    plot_average_curves( dataframes, ["Success rate"], ['srate'], ops, [1], 10, 20000, pattern )
    plot_average_curves( dataframes, ["Size"], ['size'], ops, [100], 10, 20000, pattern )
    plot_average_curves( dataframes, ["Precision"], ['prec'], ops, [1], 10, 20000, pattern )
    plot_average_curves( dataframes, ["F-measure"], ['fmeas'], ops, [1], 10, 20000, pattern )
    plot_average_curves( dataframes, ["Recall"], ['rec'], ops, [1], 10, 20000, pattern )

def plot_several_measures( directory, pattern, ops, collist, labellist, maxvallist, nbruns, nbgames ):
    dataframes = prepare_data( ['srate','size','prec','rec'], ops, nbruns, nbgames, directory, pattern )
    plot_average_curves( dataframes, labellist, collist, ops, maxvallist, nbruns, nbgames, pattern )


