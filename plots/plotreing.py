
import pandas as pd
import matplotlib.pyplot as plt
import os
import re

import pysake.data.measures as meas

sep = os.environ.get("sep", " ") # default to space

#### New code for Notebook generation

# Returns the prefix of the files in the results directory of the experience 'label'
# prefix could be label+nbAgents+nbIterations or just nbAgents+nbIterations
def getPrefix(results_directory,label,extension):
    prefix = ''
    for filename in os.listdir( results_directory ):
        if filename.endswith( extension ):
            if filename.startswith(label):        
                matchWithLabel = re.match('^([0-9\-NOORab]+)-(.*)$',filename)
                if matchWithLabel:
                    prefix = matchWithLabel.group(1)+'-'
            else:
                matchNoLabel = re.match('^([0-9]+\-[0-9]+)-(.*)$',filename)
                if matchNoLabel:
                    #prefix = matchNoLabel.group(2)+'-'+matchNoLabel.group(3)+'-'
                    prefix = matchNoLabel.group(1)+'-'
    return prefix

 
# Load a file, normalise columns, add F-measure
def load_norm_data ( fn, results_directory, observed_variables ):
    ocols = []
    for col in observed_variables:
        ocols.append( 'avg-'+col )
    df = pd.read_csv( results_directory+'/'+fn+".tsv", sep=sep, usecols=ocols ) #, index_col = False
        
    for att in observed_variables:
        df.rename(columns = {'avg-'+att:att}, inplace = True)
        df[att] = df[att].astype(float)

    # computes derived variables
    df['fmeas'] = meas.harmonic_mean( [df['prec'], df['rec']] )
    
    return df

# Returns an ordered array of the data for just one pattern
def prepare_pattern_data ( ops, pattern, results_directory, observed_variables ):
    dataDict = {}
    for op in ops:
        filepath = results_directory+'/'+pattern.replace( "%%", op )+".tsv"
        if os.path.isfile(filepath):
            dataDict[op] = load_norm_data( pattern.replace( "%%", op ), results_directory, observed_variables )
    return dataDict

# Prepare data for all plots
# Returns a dictionary where the keys are combinations of an experience+pattern
# Used for first attempt to manage small specia cases
def prepare_data_for_labels(labelpatts,base_directory,observed_variables):
    dataDict = {}
    for [label,pattern] in labelpatts:
        display('labels')
        display(label)
        display(pattern)
        filepattern = label+pattern
        if label == '20170208-NOOR':
            ops = ['add','del','repl']
        elif label == '20170209-NOOR':
            ops = ['refine','addjoin','refadd']
        #This is for manage cases where there is no pattern, files concern just operators
        opPattern = '%%-'
        if pattern == '':
            opPattern = '%%'                        
        #resdir = base_directory+'/../experiences/'+label+'/results/'
        resdir = base_directory+'/../'+label+'/results/'
        filesprefix = getPrefix(resdir,label,'tsv')
        #We look for the directory containing result files and also for the prefix of the files in that directory
        dd = prepare_pattern_data(ops,filesprefix+opPattern+pattern,resdir,observed_variables)
        dataDict[filepattern] = dd
    return(dataDict)

# Prepare data needed for all plots
# Returns a dictionary where the keys are combinations of an experience+pattern
def prepare_all_data(expPatterns,results_directory,labelexp,prefix,ops,observed_variables):
    # a dict of expPatterns: {'20180601-NOOR': ['clever-nr-gen', 'clever-nr-im80-gen'], '20180529-NOOR': ['clever-nr'], '20180530-NOOR': ['clever-nr-im80']}
    dataDict = {}
    for label, patterns in expPatterns.items():
        #an item of expPatterns: '20180601-NOOR': ['clever-nr-gen', 'clever-nr-im80-gen']
        #label = '20180601-NOOR
        # patterns = ['clever-nr-gen', 'clever-nr-im80-gen']        
        #display('Will deal with patterns of '+label)
        for patt in patterns:
                #display('Will add pattern '+patt)
                filepattern = label+patt
                #display(filepattern+'--')
                if not (filepattern in dataDict):
                    #This is for manage cases where there is no pattern, files concern just operators
                    opPattern = '%%-'
                    if patt == '':
                        opPattern = '%%'                        
                    #We look for the directory containing result files and also for the prefix of the files in that directory
                    # LABEL+NBAGENTS+ITERATIONS or just NBAGENTS+ITERATIONS
                    filesprefix = prefix
                    #display('prefix:'+prefix)
                    resdir = results_directory
                    if not label == labelexp:
                        local_directory = os.getcwd()
                        resdir = local_directory+'/../'+label+'/results/'
                        #resdir = local_directory+'/../experiences/'+label+'/results/'
                        filesprefix = getPrefix(resdir,label,'tsv')
                        if label == '20170208-NOOR':
                            ops = ['add','del','repl']
                        elif label == '20170214a-NOOR':
                            ops = ['delete','replace','add']
                        elif label == '20170214b-NOOR':
                            ops = ['refine','addjoin','refadd']
                        elif label == '20170222-NOOR':
                            ops = ['replace']
                        #This also works for resdir
                        #resdir = results_directory+'/../../'+filepair[0]+'/results/'
                    #display("Will prepare dataframe for "+filesprefix+patt+" in "+resdir)
                    if (labelexp == '20170208-NOOR'):
                        ops = ['add','del','repl']
                    df = prepare_pattern_data(ops,filesprefix+opPattern+patt,resdir,observed_variables)
                    dataDict[filepattern] = df

    return(dataDict)
     
def getCurve( datadict, expserie, operator ):
    return datadict[expserie][operator]
    
def plot_datafiles_curve( alldata, indexes, ops, ylabel, xlabel, xmax, ymax, col, nbgames ):
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    linestylelist = ['solid', 'dashed', 'dotted', 'dashdot']
    
    for index, op, ls in zip( indexes, ops, linestylelist ):
        plt.gca().set_prop_cycle(None)
        #print(alldata[index][op].iloc[1::50])
        getCurve( alldata, index, op ).iloc[1::100].plot(kind='line',y=col, label=op, ax=ax, linestyle=ls, linewidth=1.0) #, marker='*', color='blue'
    ax.set_xlabel(xlabel)  # Add an x-label to the axes.
    ax.set_xlim([0,xmax])
    ax.set_ylabel(ylabel)  # Add a y-label to the axes.
    ax.set_ylim([0,ymax])
    #ax.set_ylim([0,100])
    ax.spines['top'].set_visible(False) # suppress top and right bars
    ax.spines['right'].set_visible(False)
    #
    #ax.set_title(label+" for "+pattern)  # Add a title to the axes.
    ax.set_title(ylabel)  # Add a title to the axes.
    #ax.legend(loc='center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=6)  # Add a legend.
    #ax.legend() # ncol=2
    lines = ax.get_lines()
    plt.show()

def plot_average_curve( alldata, indexes, ylabel, xlabel, xmax, ymax, col, ops, legend=True, extralgd=[] ):
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    linestylelist = ['solid', 'dashed', 'dotted', 'dashdot']
    lgd2ln = []
    for index, ls in zip( indexes, linestylelist ):
        plt.gca().set_prop_cycle(None)
        lgdln = []
        for op in ops:
            #add 100 points to plot on intervals of 100
            getCurve( alldata, index, op ).iloc[1::100].plot(kind='line',y=col, label=op, ax=ax, linestyle=ls, linewidth=1.0, legend=legend) #, marker='*', color='blue'
        lgd2ln += [ax.lines[-1]]
    ax.set_xlabel(xlabel)  # Add an x-label to the axes.
    ax.set_xlim([0,xmax])
    ax.set_ylabel(ylabel)  # Add a y-label to the axes.
    ax.set_ylim([0,ymax])
    ax.spines['top'].set_visible(False) # suppress top and right bars
    ax.spines['right'].set_visible(False)
    if legend == True:
        lgdln = ax.lines[0:len(ops)]
        lg1 = ax.legend( handles=lgdln, loc='lower center', bbox_to_anchor=(0.5, 1.05), ncol=6 )
        if extralgd != []:   # and ldg2ln not 1
            plt.legend( lgd2ln, extralgd, loc='lower center', bbox_to_anchor=(0.5, -.4), ncol=len(lgd2ln) )
            ax.add_artist(lg1)
    plt.show()

# Plots a series of several measures for a set of operators, in the same frame
# pattern = [ measure, LABEL, suffix, label, ymax ]
# Currently only works if all measures have the same ymax (otherwise add a second scale)
def plot_several_measures( alldata, patterns, xmax, ops, ylabel='', legend=True, topLegend=False, botLegend=False ):
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    linestylelist = ['solid', 'dashed', 'dotted', 'dashdot']
    zlabel = ''
    lgd2ln = []
    extralgd = []
    for pattern, ls in zip( patterns, linestylelist ):
        #measure = pattern[0]
        [measure,col] = dict_ops[pattern[0]]
        index = pattern[1]+pattern[2]
        if zlabel != '':
            zlabel += ' / '
        zlabel += pattern[3]
        ymax = pattern[4]
        plt.gca().set_prop_cycle(None)
        lgdln = []
        extralgd += [pattern[3]]
        for op in ops:
            #add 100 points to plot on intervals of 100
            getCurve( alldata, index, op ).iloc[1::100].plot(kind='line',y=col, label=op, ax=ax, linestyle=ls, linewidth=1.0, legend=legend) #, marker='*', color='blue'
        lgd2ln += [ax.lines[-1]]
    # Axis
    ax.set_xlabel('Games')  # Add an x-label to the axes.
    ax.set_xlim([0,xmax])
    if ylabel != '':
        ax.set_ylabel(ylabel)  # Add a y-label to the axes.
    else:
        ax.set_ylabel(zlabel)
    ax.set_ylim([0,ymax])
    ax.spines['top'].set_visible(False) # suppress top and right bars
    ax.spines['right'].set_visible(False)
    if legend == True or topLegend == True:
        lgdln = ax.lines[0:len(ops)]
        lg1 = ax.legend( handles=lgdln, loc='lower center', bbox_to_anchor=(0.5, 1.05), ncol=6 )
#??        if extralgd != []:   # and ldg2ln not 1
        ax.add_artist(lg1)
    if legend == True or botLegend == True:
        if extralgd != []:   # and ldg2ln not 1
            plt.legend( lgd2ln, extralgd, loc='lower center', bbox_to_anchor=(0.5, -.4), ncol=len(lgd2ln) )
    plt.show()

# Plots several curves, in the same frame
# pattern = [ measure, LABEL, suffix, label, ymax, ops ]
# Ops is the list of _index_ of the operator in the table!
# This is unreadable and may depend on how the table has been loaded!
# Definitelly: the problem is that the df des not store the rank of plots...
# data_rank( LABEL, XXX, op ) -> int
# Currently only works if all measures have the same ymax (otherwise add a second scale)
def plot_several_curves( alldata, patterns, xmax, ylabel='', legend=True ):
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    for pattern in patterns:
        [measure,col] = dict_ops[pattern[0]]
        index = pattern[1]+pattern[2]
        zlabel = pattern[3]
        ymax = pattern[4]
        for op in pattern[5]:
            getCurve( alldata, index, op ).iloc[1::100].plot(kind='line',y=col, label=zlabel, ax=ax, linewidth=1.0) #, marker='*', color='blue'
    # Axis
    ax.set_xlabel('Games')  # Add an x-label to the axes.
    ax.set_xlim([0,xmax])
    if ylabel != '':
        ax.set_ylabel(ylabel)  # Add a y-label to the axes.
    else:
        ax.set_ylabel(zlabel)
    ax.set_ylim([0,ymax])
    ax.spines['top'].set_visible(False) # suppress top and right bars
    ax.spines['right'].set_visible(False)
    if legend == True:
        ax.legend( loc='lower center', bbox_to_anchor=(0.5, 1.05), ncol=6 )
    plt.show()

# JE 2023: That would be far better to have simple notations:
#dict_ops = {'Size':['Size','size'],'size':['Size','size'],'Success\\ rate':['Succes rate','srate'],'Success~rate':['Succes rate','srate'],'Success$~$rate':['Succes rate','srate'],'Success':['Succes rate','srate'],'F$-$measure':['F-measure','fmeas'],'Prec':['Precision','prec'],'Precicion':['Precision','prec'],'Precision':['Precision','prec'],'Rec':['Recall','rec'],'Recall':['Recall','rec']}
dict_ops = {'size':['Size','size'],'successrate':['Success rate','srate'],'f-measure':['F-measure','fmeas'],'precision':['Precision','prec'],'recall':['Recall','rec']}

def print_datafiles( alldf, plotinfo, labelpatts, cols, nbiter ):
    [plotlabel,col] = dict_ops[plotinfo[0]]
    xlabel = plotinfo[1]
    xmax = int(plotinfo[2])
    ymax = int(plotinfo[3])
    indexes = []
    for labpat in labelpatts:
        indexes.append(labpat[0]+labpat[1])
    #plot_datafiles_curve_data( alldf, indexes, cols )
    plot_datafiles_curve( alldf, indexes, cols, plotlabel, xlabel, xmax, ymax, col, nbiter )

def print_series( alldf, measures, patterns, nbgames, maxsize, operators, extralgd=[] ):
    for m in measures:
        newpatterns = []
        if m == 'size':
            for patt in patterns:
                newpatterns += [[m] + patt +[ patt[0]+' ('+patt[1]+')', maxsize]]
        else:
            for patt in patterns:
                newpatterns += [[m] + patt +[ patt[0]+' ('+patt[1]+')', 1]]
        if m == measures[0]:
            plot_several_measures( alldf, newpatterns, nbgames, operators, ylabel=dict_ops[m][0], legend=False, topLegend=True )
        elif m == measures[-1]:
            plot_several_measures( alldf, newpatterns, nbgames, operators, ylabel=dict_ops[m][0], legend=False, botLegend=True )
        else:
            plot_several_measures( alldf, newpatterns, nbgames, operators, ylabel=dict_ops[m][0], legend=False )

# This is to be obsoleted because it can be replaced by plot_several_measures or plot_several_curves
def print_one_series( alldf, plotinfo, patterns, operators, legend=True, extralgd=[] ):
    indexes = []
    for pattern in patterns:
        indexes.append(pattern[0]+pattern[1])
    [plotlabel,col] = dict_ops[plotinfo[0]]
    xlabel = plotinfo[1]
    xmax = int(plotinfo[2])
    ymax = int(plotinfo[3])
    plot_average_curve( alldf, indexes, plotlabel, xlabel, xmax, ymax, col, operators, legend, extralgd=extralgd )

### Print all plots of an experience extracted from the latexfile
def print_all_series( allplots, alldf, operators ):
    for group in allplots:
        for plot in group:
            indexes = []
            for pattern in plot[1:]:
                indexes.append(pattern[0]+pattern[1])
            plotinfo = plot[0]
            [plotlabel,col] = dict_ops[plotinfo[0]]
            xlabel = plotinfo[1]
            xmax = int(plotinfo[2])
            ymax = int(plotinfo[3])
            plot_average_curve( alldf, indexes, plotlabel, xlabel, xmax, ymax, col, operators )
