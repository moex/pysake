
import pandas as pd
import matplotlib.pyplot as plt
import os

import pysake.data.measures as meas

sep = os.environ.get("sep", " ") # default to space

##### All this is legacy code from Clarisse
 
 # Load a file, normalise columns, add F-measure
def load_norm_data ( fn, results_directory, observed_variables ):
    ocols = []
    for col in observed_variables:
        ocols.append( 'avg-'+col )
    df = pd.read_csv( results_directory+'/'+fn+".tsv", sep=sep, usecols=ocols ) #, index_col = False
        
    for att in observed_variables:
        df.rename(columns = {'avg-'+att:att}, inplace = True)
        df[att] = df[att].astype(float)

    # computes derived variables
    df['fmeas'] = meas.harmonic_mean( [df['prec'], df['rec']] )
    
    return df

# Returns an ordered array of the data
def prepare_simple_data ( ops, nbgames, pattern_list, results_directory, observed_variables ):
    """
        Return a DataFrame containing the 
    Parameters:
    :ops -- the adaptation operators to print
    :nbgames -- the number of game played to display    
    :pattern_list -- the patterns characterising the files to upload
    :results_directory -- the directory where to find the result files
    :observed_variables -- the column to extract from the result files
    """
    dataArray = []
    for pattern in pattern_list:
        for op in ops:
            dataArray.append( load_norm_data( pattern.replace( "%%", op ), results_directory, observed_variables ) )
    return dataArray
        
def plot_average_curve( data, label, col, ops, maxval, nbgames, pattern_list, legend=True ):
    """
        Plots Size, Success rate, Precision, F-measure and Recall for different experiments and different modalities
    Parameters:
    :data -- the DataFrame from which to extract the data
    :label -- the label of the plot
    :col -- the column from which the measure is taken
    :ops -- the adaptation operators to print
    :maxval -- the maximal value of the y axis
    :pattern_list -- the patterns characterising the files to upload
    :legend -- do we print the legend or not
    """
    fig = plt.figure(figsize=(12,3))
    ax = plt.gca()
    linestylelist = ['solid', 'dashed', 'dotted', 'dashdot']
    ind = 0
    for pattern, ls in zip( pattern_list, linestylelist ):
        plt.gca().set_prop_cycle(None)
        for op in ops:
            data[ind].iloc[1::100].plot(kind='line',y=col, label=op, ax=ax, linestyle=ls, linewidth=1.0) #, marker='*', color='blue'
            ind += 1
    ax.set_xlabel('games')  # Add an x-label to the axes.
    ax.set_xlim([0,nbgames])
    ax.set_ylabel(label)  # Add a y-label to the axes.
    ax.set_ylim([0,maxval])
    #ax.set_ylim([0,100])
    ax.spines['top'].set_visible(False) # suppress top and right bars
    ax.spines['right'].set_visible(False)
    #
    #ax.set_title(label+" for "+pattern)  # Add a title to the axes.
    ax.set_title(label)  # Add a title to the axes.
    #ax.legend(loc='center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=2)  # Add a legend.
    if legend == True:
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3), ncol=6) # ncol=2
    #
    plt.show()

def print_data_series( pattern_list, nbiter, results_directory, observed_variables ):
    """
        Plots Size, Success rate, Precision, F-measure and Recall for different experiments and different modalities
    Parameters:
    :pattern_list -- the patterns characterising the files to upload
    :nbiter -- the number of game played to display
    :results_directory -- the directory where to find the result files
    :observed_variables -- the column to extract from the result files
    """
    allops = ['delete','replace','refine','add','addjoin','refadd']
    dataframes = prepare_simple_data( allops, nbiter, pattern_list, results_directory, observed_variables )
    # here it would be good to scrap the MAXSIZE. This can be done in the data
    plot_average_curve( dataframes, "Size", 'size', allops, 120, nbiter, pattern_list, False )
    plot_average_curve( dataframes, "Success rate", 'srate', allops, 1, nbiter, pattern_list, False )
    plot_average_curve( dataframes, "Precision", 'prec', allops, 1, nbiter, pattern_list, False )
    plot_average_curve( dataframes, "F-measure", 'fmeas', allops, 1, nbiter, pattern_list, False )
    plot_average_curve( dataframes, "Recall", 'rec', allops, 1, nbiter, pattern_list, True )


