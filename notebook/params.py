from configobj import ConfigObj
import re

def read_parameter_file( path ):
    instring = False
    with open( path, 'r' ) as file:
        all_lines = []
        buffer = ''
        # loop through all lines using f.readlines() method
        for line in file.readlines():
            for i in range(0, len(line)):
                if line[i] == '"' and (i == 0 or line[i-1] != '\\') and not instring:
                    instring = True
                elif line[i] == '"' and (i == 0 or line[i-1] != '\\') and instring:
                    instring = False
            if not instring:
                all_lines.append( buffer+(line[:-1]) )
                buffer = ''
            else:
                buffer += line[:-1]+"___"
    config = ConfigObj( all_lines, list_values=False, interpolation='template')
    # It is possible to find this in old params.sh
    if 'OPLIST' in config:
        opl = config["OPLIST"]
        if opl == '`echo ${OPS} | sed "s: :/:g"`':
            config['OPLIST'] = opl.replace( ' ', '/' )
    return config

def get_parameter( params, key, default='' ):
    """
    Returns a parameter.
    :params -- the parameter dictionary.
    :key -- the name of the parameter.
    :default -- default value.
    """

    # JE returns the parameter, '' if not present
    value = params.get( key, default )
    if value.startswith('\"') and value.endswith('\"'):
            value = value[1:-1]
    return value.replace( '___', '\n' )

def set_link_labels( text ):
    return set_link_labels_gen( text, True )

def set_html_link_labels( text ):
    return set_link_labels_gen( text, False )

# mkdn=True: markdown, otherwise HTML
def set_link_labels_gen( text, mkdn ):
    #regExp = '^(.*?)(\[?\[?[0-9]{8}(a|b)?-NOOR\]?\]?)(.*)$'
    regExp = r"^(.*?)(\[?\[?[0-9]{8}(a|b)?-NOOR\]?\]?)(.*)$"
    lookForLabels = True;
    newText = ''
    while lookForLabels:
        match = re.match(regExp,text)
        if match:
            label = match.group(2)
            pos = text.find(label)
            newText += text[:pos]
            text = text[pos+len(label):]
            if label.startswith('['):
                label = label[1:]
            if label.endswith(']'):
                label = label[:-1]
            if mkdn:
                newText += "[["+label+"](../"+label+")]"
            else:
                newText += '[<a href="../'+label+'">'+label+'</a>]'
        else:
            newText += text
            lookForLabels = False
    return newText

def htmlResultAndStatus( result, status, desc, ref ):
    line1 = '<b>'+result+'</b>'
    line2 = ''
    if ref:
        line1 += ' ['+ref+']' # Hypertext too complex (moex/exmo)
    if status == "VALID":
        line2 = "" # lightgreen
    elif status == "INVALID":
        line2 = '<span style="background-color: red;">' + set_html_link_labels( desc ) + '</span>'
    elif status == "UNCERTAIN":
        line2 = '<span style="background-color: orange;">' + set_html_link_labels( desc ) + '</span>'
    elif status == "PARTLY":
        line2 = '<span style="background-color: pink;">' + set_html_link_labels( desc ) + '</span>'
    elif status == "SUBSUMED":
        line2 = '<span style="background-color: lightblue;">Repeated in '+ set_html_link_labels( desc ) +'</span>'
    if line2:
        line1 += '<br />'+line2
    return line1
