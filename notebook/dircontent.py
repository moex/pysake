
from IPython.display import display, HTML, Code, Markdown, FileLink, FileLinks

import pandas as pd

import os
import re

## Directory printing functions

def display_flat_directory(directory):
    """
    Displays the content of the result directory.
    Good to use when there is only a simple directory.
    Parameters:
    :directory -- the result directory of which the content is displayed
    """

    display( FileLinks( directory, result_html_suffix='<br />', recursive=True ) )

# This one is better for factorial plans (or when there are subdirectories)
def display_factorial_results(directory):
    """
    Displays the content of the result directory.
    Good to use when there is only a simple directory.
    Parameters:
    :directory -- the result directory of which the content is displayed
    """

    idcount=0
    for d in directories(directory):
        display(HTML("<li> "+d+" <button onclick=\"togglev('qu"+str(idcount)+"')\">+/-</button>"))
        RAWHTML = "<div id=\"qu"+str(idcount)+"\">"  #  style=\"display: none;\"
        for filename in os.listdir(d):
            RAWHTML += "&bullet; <a href='"+d+filename+"'>"+filename+"</a>"
        display(HTML(RAWHTML+"</div></li>"))
        idcount = idcount+1


# JE: Should be replaced by:
#with os.scandir( root ) as it:
#    for entry in it:
#        if entry.is_dir():
#            print( root + entry.name )
def directories(root):
    for filename in os.listdir(root):
        if '.txt' not in filename:
            yield root + filename + '/'

# ??
def get_dirs(root, criteria):
    for d in directories(root):
        m = read_params(d)
        if match(criteria, m):
            yield d

# ??
def match(criteria, params):
    cIn, cOut = {}, {}
    if 'in' in criteria:
        cIn = criteria['in']
    if 'out' in criteria:
        cOut = criteria['out']
    for key in params:
        if key in cIn:
            ins = [str(c) for c in cIn[key]]
        if key in cOut:
            outs = [str(c) for c in cOut[key]]
        if key in cIn and str(params[key]) not in ins:
            return False
        if key in cOut and str(params[key]) in outs:
            return False
    return True

# JE: why not use the df instead? df['x'] should do?
def read_params(directory, sep=' '):
    df = pd.read_csv(directory + 'parameters.csv', index_col=0, header=None, sep=sep).T
    m = {}
    for k in df.keys():
        m[k] = df[k].values[0]
    return m


def get_runs(root, sep=' '):
    for d in directories(root):
        m = read_params(d, sep)
        for filename in os.listdir(d):
            if "run" not in filename:
                continue

            run = re.search("run([0-9]+)", filename).groups(1)[0]
            m2 = m.copy()
            m2["run"] = run
            dataframe = pd.read_csv(d + filename, sep=sep)
            yield m2, dataframe

