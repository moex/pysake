import argparse
import pandas as pd
import os
import re

import pysake.notebook.dircontent as dc

# JE: apparently, python has no soluton for that!
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


class Data(object):

    def __init__(self, directory, sep=' ') -> None:
        super().__init__()
        if directory[-1] != '/':
            directory += '/'
        self.directory = directory
        self.variables = None
        self.sep = sep
        self.conditions = []
        self.aggregate_groups = []

    def copy(self):
        d = Data(self.directory, self.sep)
        return d

    def get_rows(self):
        transpose = False
        for d in dc.directories(self.directory):
            m = dc.read_params(d, self.sep)
            for filename in os.listdir(d):
                if "run" not in filename:
                    continue

                run = re.search("run([0-9]+)", filename).groups(1)[0]

                df = pd.read_csv(d + filename, sep=self.sep)
                if transpose:
                    df = df.T

                for index, row in df.iterrows():
                    m2 = m.copy()
                    m2["run"] = run
                    for key in row.keys():
                        m2[key] = row[key]
                    yield m2

    def get_data_frame(self):
        transpose = False
        all_d = {}
        for m in self.get_rows():
            for k in m:
                if k not in all_d:
                    all_d[k] = []
                all_d[k] += [str(m[k])]

        df = pd.DataFrame.from_dict(all_d)
        return df


class Project(Data):

    def __init__(self, data: Data, variables: str) -> None:
        super().__init__(data.directory)
        self.data = data
        self.variables = variables.split(";")

    def get_rows(self):
        for m in self.data.get_rows():
            m2 = {}
            for v in self.variables:
                if v in m:
                    m2[v] = m[v]
            yield m2


class Condition(Data):

    def __init__(self, data: Data, c: str) -> None:
        super().__init__(data.directory)
        self.data = data
        self.condition = c

    def get_rows(self):
        for m in self.data.get_rows():
            if self.satisfy(m):
                yield m

    def satisfy(self, m: dict):
        for k in m:
            if is_number(m[k]):
                script = k + " = float(m['" + k + "'])"
            else:
                script = k + " = m['" + k + "']"
            exec(script)
        return eval(self.condition)


class Aggregate(Data):
    class Aggregator(object):
        def __init__(self, groups: list) -> None:
            super().__init__()
            groups.sort()
            self.groups = groups

        def get_key(self, m: dict):
            key = ""
            for k in self.groups:
                key = m[k] + ";"
            return key

        def add(self, m: dict):
            pass

        def get_rows(self):
            pass

    class Avg(Aggregator):
        def __init__(self, groups) -> None:
            super().__init__(groups)
            self.computations = {}
            self.dicts = {}

        def add(self, m: dict):
            key = self.get_key(m)
            if key not in self.computations:
                self.computations[key] = 0
                self.dicts[key] = dict((k, m[k]) for k in self.groups)

        def get_rows(self):
            pass

    def __init__(self, data: Data, groups: str, func: str) -> None:
        super().__init__(data.directory)
        self.data = data
        self.groups = groups.split(";")
        self.func = func

    def get_rows(self):
        for m in self.data.get_rows():
            m2 = {}
            for v in self.variables:
                if v in m:
                    m2[v] = m[v]
            yield m2


class Union(Data):

    def __init__(self, data1: Data, data2: Data) -> None:
        super().__init__(data1.directory)
        self.data1 = data1
        self.data2 = data2

    def get_rows(self):
        for m in self.data1.get_rows():
            yield m

        for m in self.data2.get_rows():
            yield m


def condition(data: Data, c: str):
    return Condition(data, c)


def project(data: Data, variables: str):
    return Project(data, variables)


def aggregate(data: Data, groups: str, func: str):
    return Aggregate(data, groups, func)


def union(data1: Data, data2: Data):
    return Union(data1, data2)


def get(formatting: str, root: str, s: str) -> pd.DataFrame:
    data = Data(root, sep=s)
    data = eval(formatting)
    return data.get_data_frame()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--formatting", help="A string describing how data should be retrieved from all runs "
                                                   "dataset.\n "
                                                   "Use the following language:\n"
                                                   "S->F(data,<args>)|F(S,<args>)\n"
                                                   "F and <args> are predefined functions and their arguments:\n"
                                                   "project(<data>, '<variables>') where <variables> is a string "
                                                   "containing parameters/measures seperated by ';' to keep in data.\n"
                                                   "EXAMPLE: project(data,'nbAgents;ssrate;fm)\n"
                                                   "condition(<data>,'<condition>') where <condition> is a string that "
                                                   "restricts the values of any parameter/measure. (python syntax)\n"
                                                   "EXAMPLE: condition(data,'rank==10000 and nbAgents==4')\n"
                                                   "aggregate(<data>, '<groups>', '<func>') where <groups> is a string "
                                                   "containing parameters/measures seperated by ';' by which aggregations "
                                                   "are grouped.\n"
                                                   "EXAMPLE: aggregate(data,'nbAgents;run','avg')\n"
                                                   "<func> is the aggregation function to apply on the values.\n"
                                                   "union(<data1>, <data2>) concatenates rows of <data1> and <data2>."
                        , required=True)

    parser.add_argument("-s", "--sep", help="Separator used in tsv/csv files\n"
                                            "default space is used")

    parser.add_argument("-d", "--directory", help="input directory where dataset is stored", required=True)

    parser.add_argument("-o", "--output", help="output directory where file will be stored, default 'statsAnalysis'")

    parser.add_argument("-fn", "--filename", help="output file name, default 'stats.csv'")

    args = parser.parse_args()
    formatting = args.formatting
    root_directory = args.directory
    if root_directory[-1] != '/':
        root_directory += '/'
    out_directory = "statsAnalysis/"
    filename = 'stats.csv'
    sep = ' '

    if args.sep:
        sep = args.sep
    if args.filename:
        filename = args.filename
    if args.output:
        out_directory = args.output
        if out_directory[-1] != '/':
            out_directory += '/'

    if out_directory[:-1] not in os.listdir("."):
        os.mkdir(out_directory)

    df = get(formatting, root_directory, sep)
    print(df)
    df.to_csv(out_directory+filename, sep=sep)
