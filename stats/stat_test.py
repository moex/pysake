import argparse
import os

import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.stats import ttest_ind
from scipy.stats import ttest_rel
from statsmodels.formula.api import ols
from statsmodels.stats.multicomp import MultiComparison
from IPython.display import display

"""
Errors/Exceptions
"""


class MoreThanTwoSamplesTTest(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


"""
Latex output 
"""


def dataframe_to_latex(df: pd.DataFrame, p: str):
    with open(p, 'w') as tf:
        col_format = "|"
        for k in df.keys():
            col_format += "c|"
        tf.write(df.to_latex(index=False, column_format=col_format))


"""
Tests to perform
"""


def anova_post_hoc2(dependent, independent, df):
    results = {}
    for dep in dependent:
        results[dep] = {}
        for indep in independent:
            mc = MultiComparison(df[dep], df[indep])
            res = mc.tukeyhsd()
            # print(res.confint)
            results[dep][indep] = res
    return results


def anova_post_hoc(dependent, independent, df):
    results = {}
    for dep in dependent:
        results[dep] = {}
        for indep in independent:
            mc = MultiComparison(df[dep], df[indep])
            res = mc.tukeyhsd()
            results[dep][indep] = pd.DataFrame(data=res._results_table.data[1:], columns=res._results_table.data[0])
    return results


def anova(dependent, independent, df, right_formula=None, inter="+"):
    if right_formula is None:
        right_formula = inter.join(['C(Q("' + ind + '"))' for ind in independent])

    results = {}
    for dep in dependent:
        df[dep] = pd.to_numeric(df[dep])
        model = ols('Q("' + dep + '") ~ ' + right_formula, df).fit()
        res = sm.stats.anova_lm(model)
        results[dep] = res
    return results


def anova_output_org(anova_results, posthoc_results, out_directory):
    for dep in anova_results:
        # print("**************************************")
        # print("results for " + dep)
        # print("**************************************")
        # print(anova_results[dep])
        fname = "anova-"+dep+".tex"
        dataframe_to_latex(anova_results[dep], out_directory+fname)
        for indep in posthoc_results[dep]:
            # print("--------------------------------------")
            # print("post-hoc test for " + indep)
            # print("--------------------------------------")
            # print(posthoc_results[dep][indep])
            tukeydf = posthoc_results[dep][indep]
            fname = "tukeyhsd-" + dep + "-" + indep + ".tex"
            dataframe_to_latex(tukeydf, out_directory + fname)


def anova_output(independent_variables, dependent_variables, df, out_directory, inter="+"):
    anova_results = anova(dependent_variables, independent_variables, df, inter=inter)
    posthoc_results = anova_post_hoc(dependent_variables, independent_variables, df)
    anova_output_org(anova_results, posthoc_results, out_directory)
    return anova_results, posthoc_results

def color_significant_green(val):
    """
    Takes a scalar and returns a string with
    the css property `'color: red'` for negative
    strings, black otherwise.
    """
    color = 'green' if val == "True" else 'red'
    return 'color: %s' % color

# JE: added this
# Would be nicer to create a dataframe and just display it
# Super good examples:
# https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html
def display_variable_influence ( var, anova_results, dependent_variables=None ):
    df = pd.DataFrame( columns = [ 'PR(>F)', 'Significance' ] )
    key = 'C(Q("' + var + '"))'
    if dependent_variables is None:
        dependent_variables = anova_results
    #print( "variable\tPR(>F)\tSignificance")
    for dep in dependent_variables:
        val = anova_results[dep].loc[ key, 'PR(>F)']
        if val < 0.01:
            sig = "True"
        else:
            sig = "False"
        df.loc[dep] = [ val, sig ]
        #print("{0:s}\t{1:8.2f}\t{2:s}".format(dep,val,sig))
    s = df.style.applymap( color_significant_green, subset=['Significance'] )
    display( s )

def ttest(test, independent_variables, dependent_variables, df, out_directory):
    dataframes = {}
    for indep in independent_variables:
        values = list(set(df[indep].values))
        if len(values) != 2:
            raise MoreThanTwoSamplesTTest()
        data = {
            "dependent/independent": [],
            "t-value": [],
            "p-value": []
        }
        for dep in dependent_variables:
            s1 = df[df[indep] == values[0]][dep].values.astype(np.float)
            s2 = df[df[indep] == values[1]][dep].values.astype(np.float)
            results = test(s1, s2)
            data["dependent/independent"].append(dep + "/" + indep)
            data["t-value"].append(results[0])
            data["p-value"].append(results[1])
        dataframes[indep] = pd.DataFrame.from_dict(data)
    return dataframes
    # for d in dataframes:
    #     print(d)
    # dataframes_to_latex(dataframes, out_directory+"ttest_result.tex")


def ind_ttest(independent_variables, dependent_variables, df, out_directory):
    results = ttest(ttest_ind, independent_variables, dependent_variables, df, out_directory)
    return results


def ind_ttest_output(independent_variables, dependent_variables, df, out_directory):
    results = ind_ttest(independent_variables, dependent_variables, df, out_directory)
    for r in results:
        path = out_directory + "ind_ttest" + r + ".tex"
        dataframe_to_latex(results[r], path)
    return results


def rel_ttest(independent_variables, dependent_variables, df, out_directory):
    results = ttest(ttest_rel, independent_variables, dependent_variables, df, out_directory)
    return results


def rel_ttest_output(independent_variables, dependent_variables, df, out_directory):
    results = rel_ttest(independent_variables, dependent_variables, df, out_directory)
    for r in results:
        path = out_directory + "rel_ttest" + r + ".tex"
        dataframe_to_latex(results[r], path)
    return results


def normality_output(independent_variables, dependent_variables, df, out_directory):
    for indep in independent_variables:
        values = list(set(df[indep].values))
        if len(values) != 2:
            raise Exception()
        for dep in dependent_variables:
            s1 = df[df[indep] == values[0]][dep].values
            s2 = df[df[indep] == values[1]][dep].values
            results = ttest_ind(s1, s2)


if __name__ == "__main__":
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-dv", "--dependent", help="Dependent variables seperated by \n", required=True)

    parser.add_argument("-i", "--independent", help="Independent variables seperated by ';'", required=True)

    parser.add_argument("-t", "--test", help="Statistical test to perform\n"
                                             "can be one of: ANOVA, t-test, pt-test, normality", required=True)

    parser.add_argument("-s", "--sep", help="Separator used in tsv/csv files\n"
                                            "default space is used")

    parser.add_argument("-p", "--path", help="input path to file where data is stored", required=True)

    parser.add_argument("-o", "--output", help="output directory where files will be stored, default 'statsAnalysis'")

    args = parser.parse_args()
    independent_vars = args.independent.split(";")
    dependent_vars = args.dependent.split(";")
    t = args.test
    in_path = args.path
    out_dir = "statsAnalysis/"
    s = ' '

    if args.sep:
        sep = args.sep
    if args.output:
        out_dir = args.output
        if out_dir[-1] != '/':
            out_dir += '/'

    if out_dir[:-1] not in os.listdir("."):
        os.mkdir(out_dir)

    """
    get summary of data in form independent variables/ dependent variables
    """

    data = pd.read_csv(in_path, index_col=0, sep=s)

    """
    applying the test
    """
    tests = {
        "anova": anova_output,
        "t-test": ind_ttest,
        "pt-test": rel_ttest,
        "normality": normality_output
    }
    print("performing " + t)
    tests[t](independent_vars, dependent_vars, data, out_dir)
