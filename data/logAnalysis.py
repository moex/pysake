# Open file
# Read line by line those containing ^.* main [^\t]*
# as W\ (a\ =\ v{;})*
# To store as a line
# Average (as float) all those lines with the same W

import re
import pandas as pd
import pysake.data.tableDisplay as tdis

def parseLogfile( filename, cols ):
    """
    Parse a log file and returns a dataframe with average attribute values aggregated by modality
    Parameters:
    :filename -- the name (path) of the log file
    :cols -- the list of column names of the resulting table
    """
    logfile = open( filename, 'r' )
    entries = []
    for line in logfile:        
        # Unsure it will always be DEBUG...
        twosides = re.split(r'^.*DEBUG main [^\s]*\s+',line.strip())
        if len(twosides) > 1:
            tug = re.split( r"\s", twosides[1].strip(), maxsplit=1 )
            listatt = re.findall( r'\s*\w+ = ([^;]*)[;]?', tug[1] )
            entries += [ [tug[0]]+listatt ]
    logfile.close()
    df = pd.DataFrame( entries, columns=cols )
    tdis.typeTableColumns( df )
    df = df.groupby(cols[0]).mean()
    return df
