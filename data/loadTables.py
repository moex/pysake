import pandas as pd

def loadPlot( filename, variables, sep='\t' ):
    """
        Loads a single result file
    The first line is suppressed
    Columns are ranks + variables (with min-/avg-/max-)
    Parameters:
    :filename -- the file
    :variables -- the observed variables
    :sep -- separator in the files
    """
    # create the list
    cols = ['rank']
    for meas in variables:
        cols += ['min-'+meas, 'avg-'+meas, 'max-'+meas]
    # Just load the table...
    df = pd.read_csv( filename, sep=sep, names =  cols )
    df = df.iloc[1:]
    return df
    
