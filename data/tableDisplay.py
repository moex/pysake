
from collections import OrderedDict
import pandas as pd # Only for categorical

###### TYPING

def typeTableColumns( df ):
    if 'size' in df.columns: 
        df['size'] = df['size'].astype(float)
    if 'srate' in df.columns: 
        df['srate'] = df['srate'].astype(float)
    if 'rec' in df.columns: 
        df['rec'] = df['rec'].astype(float)
    if 'prec' in df.columns: 
        df['prec'] = df['prec'].astype(float)
    if 'conv' in df.columns: 
        df['conv'] = df['conv'].astype(int)
    if 'fmeas' in df.columns: 
        df['fmeas'] = df['fmeas'].astype(float)
    if 'inc' in df.columns: 
        df['inc'] = df['inc'].astype(float)
    if 'revisionModality' in df.columns or 'revisionModality' in df.index.names:
        df['revisionModality'] = pd.Categorical(df['revisionModality'], ["nothing", "delete", "replace", "refine", "add", "addjoin", "refadd"])
    if 'str' in df.columns:
        df['str'] = pd.Categorical(df['str'], ['_',"strgen", "strspc", "strrdm"])
    if 'strenghen' in df.index.names:
        df['strenthen'] = pd.Categorical(df['strenghen'], ['_',"strgen", "strspc", "strrdm"])
    # ??
    #if 'generative' in df.columns or 'generative' in df.index.names:
    #    df['generative'] = pd.Categorical(df['generative'], ['_NOT_','_','1'])
    #if 'expandAlignments' in df.columns or 'expandAlignments' in df.index.names:
    #    df['expandAlignments'] = pd.Categorical(df['expandAlignments'], ['_','_NOT_','memory','clever'])
    # Better keep these as strings...
    #if 'immediateRatio' in df.columns or 'immediateRatio' in df.index.names:
    #    df.replace({'immediateRatio':{'_':'0'}}, inplace = True)        
    #    df['immediateRatio'] = df['immediateRatio'].astype(float)

###### STRIPPING COLUMNS

# suppresses columns in colList and those which always have the same value

def stripColumns( df, keylist, colList ):
    for col in colList:
        df = df.drop( axis=1, columns=col )
    for key in keylist:
        if (df[key] == df[key][0]).all():
            df = df.drop(axis=1,columns=key)
    return df
    
###### ORDERING AND RENAMING

indexDict = OrderedDict([
    ('syntactic','Syntactic'),
    ('realistic','Realistic size'),
    ('startEmpty','Empty start'),
    ('repairers','Repairer'),
    ('rawRepair','Repair rate'),
    ('ontoLookup','Empty start'),
    ('im80','Relaxation'),
    ('immediateRatio','Relaxation'),
    ('gen','Generation'),
    ('generative','Generation'),
    ('clever-nr','Expansion'),
    ('expandAlignments','Expansion'),
    ('clever','Expansion (clever)'),
    ('protected','Expansion (protected)'),
    ('protected-nr','Expansion (protected-nr)'),
    ('strengthen','Strenghening'),
    ('str','Strenghening'),
    ('revisionModality','Operator'),
    ('ops','Operator'),
    ('srate','Success rate'),
    ('size','Size'),
    ('inc','Incoherence'),
    ('prec','Semantic precision'),
    ('fmeas','Semantic F-measure'),
    ('rec','Semantic recall'),
    ('conv','Convergence')])


def orderColumns( colList ):
    result = []
    for name in indexDict:
        if name in colList:
            result += [name]
    return result

def orderRenameTableStandard( df ):
    return orderRenameTable( df, indexDict )

def orderRenameTable( df, indexDict ):
    indexSelect = []
    indexNames = []
    colArray = []
    colDict = []
    sortIndex = []
    nblevels = df.index.nlevels
    for index, name in indexDict.items():
        if (nblevels > 1) and index in df.index.names:
            indexSelect += [index]
            indexNames += [name]
        else:
            if index in df.columns:
                colArray += [index]
                colDict += [(index, name)]
                if name in ['Empty start','Syntactic','Realistic size','Relaxation','Generation','Expansion','Expansion (clever)','Expansion (protected)','Expansion (protected-nr)','Strenghening','Operator']:
                    sortIndex += [name]
    # Reorder (levels and columns)
    if (nblevels > 1):
        df = df.reorder_levels( indexSelect )
    df = df[colArray]
    # Rename (levels and columns)
    if (nblevels > 1):
        df.index.names = indexNames
    df.rename( dict( colDict ), axis='columns', inplace=True )
    # Sort the rows (by index column order)
    if ( len(sortIndex) > 0 ):
        df = df.sort_values( by = sortIndex )
    return df

### FORMATTING

alteven = [ ('background-color', 'Lightgreen') ]
barright = [ ('border-right','1px solid black')]
barbelow = [ ('border-bottom','1px solid black')]
centhead = [ ('text-align', 'center'), ('color','red')]

standardStyles6 = [
  dict(selector="th:last-of-type", props=barright), 
##  dict(selector="tr:nth-child(1) th", props=centhead), 
##  dict(selector="tr:nth-child(even)", props=alteven), 
  dict(selector="tr:nth-child(6n)", props=barbelow), 
##    dict(selector="td:nth-child(6)", props=barleft),
##    dict(selector="td:nth-child(2)", props=barleft)
  ]

cellFormat = {'Size': '{:0.0f}','Success rate': '{:0.2f}','Incoherence': '{:0.2f}','Semantic precision': '{:0.2f}','Precision': '{:0.2f}','Semantic F-measure': '{:0.2f}','F-measure': '{:0.2f}','Semantic recall': '{:0.2f}','Recall': '{:0.2f}','Syntactic F-measure': '{:0.2f}','Convergence': '{:0.0f}'}

def formatTableStandard( style, size=0, start=0 ):
    standardStyles = [
        dict(selector="th:last-of-type", props=barright)
    ]
    standardStyles = [
        dict(selector="tr:nth-child("+str(start)+")", props=barbelow),
        dict(selector="tr:nth-child("+str(size)+"n+"+str(start)+")", props=barbelow)
    ]
    style.set_table_styles( standardStyles )
    style.format( cellFormat )
    return style
