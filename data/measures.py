

def harmonic_mean(x):
    """
    Computes the harmonic mean of two values.
    Parameters:
    :x -- the values in the row this function is applied to. Could also work on a list or a tuple.
    """

    return 2*x[0]*x[1]/(x[0]+x[1])


