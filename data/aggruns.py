import pandas as pd
import re
import os

import pysake.data.measures as meas
import pysake.notebook.dircontent as dc

sep = os.environ.get("sep", " ") # default to space

def rank_of_last_changing_value( df, att ):
    """
    Finds the rank of the last value in a data frame for which value of a particular column has decreased
    Parameters:
    :df -- the data frame
    :att -- the attribute name
    """

    return rank_of_last_changing_value_in_column( df, df[att] )

def rank_of_last_changing_value_in_column( df, col ):
    """
    Finds the rank of the last value in a data frame for which value of a particular column has decreased
    Parameters:
    :df -- the data frame
    :col -- the column
    """

    # If all values are the same, it should then return 0 or 1
    #n = len( pd.unique( col ) )
    if len( pd.unique( col ) ) <= 1:
        return 0
    else:
        # JE: the for-loop below is an anti-pattern
        # The expression returns the index of the last element of the df 
        # containing only the rows satisfying the condition (!= or < or >)
        return df[ col < col.shift(1) ].index[-1]
        # for j in range(len(df)-1,0,-1):
        #    if col.iloc[j] < col.iloc[j-1] :
        #        return j

def rank_of_last_decreasing_value(df,att):
    """
    Finds the rank of the last value in a data frame for which value of a particular column has decreased
    Parameters:
    :df -- the data frame
    :att -- the attribute name
    """

    # If all values are the same, it should then return 0 or 1
    #n = len( pd.unique( df[att] ) )
    if len( pd.unique( df[att] ) ) <= 1:
        return 0
    else:
        # JE: the for-loop below is an anti-pattern
        # The expression returns the index of the last element of the df 
        # containing only the rows satisfying the condition (!= or < or >)
        return df[ df[att] < df[att].shift(1) ].index[-1]
        # for j in range(len(df)-1,0,-1):
        #    if df[att].iloc[j] < df[att].iloc[j-1] :
        #        return j

def collect_all_runs( directory, observed_variables ):
    """
    Aggregates in a single table all runs of all paramenters indexed by the declared parameters
    Parameters:
    :directory: where all runs results are stored
    """
    
    all_d = {}
    for entry in os.scandir( directory ):
        if entry.is_dir():
            d = directory + "/" + entry.name + "/"
            m = dc.read_params( d, sep )
            for filename in os.listdir(d):
                if "run" not in filename:
                    continue

                # JE: is run used somewhere?
                run = re.search("run([0-9]+)", filename).groups(1)[0]

                df = pd.read_csv(d + filename, sep=sep) #, index_col = False
        
                # m2, for observed variables, will contain the last row...
                m2 = m.copy()
                for att in observed_variables:
                    m2[att] = df[att].values[-1]

                # plus those computed variables
                m2['fmeas'] = meas.harmonic_mean([df['prec'].values[-1],df['rec'].values[-1]])
                # see note above
                m2['conv'] = df['rank'].iloc[rank_of_last_changing_value(df,'srate')]

            for k in m2:
                if k not in all_d:
                    all_d[k] = []
                all_d[k] += [str(m2[k])]
    tosv = pd.DataFrame.from_dict( all_d )
    return tosv

def aggregate_all_runs( directory, observed_variables ):
    """
    Aggregates in a single table all runs of all paramenters indexed by the declared parameters
    Parameters:
    :directory: where all runs results are stored
    """
    
    all_d = {}
    for entry in os.scandir( directory ):
        if entry.is_dir():
            d = directory + "/" + entry.name + "/"
            m = dc.read_params( d, sep )
            series = []
            for filename in os.listdir(d):
                if "run" not in filename:
                    continue

                # JE: is run used somewhere?
                run = re.search("run([0-9]+)", filename).groups(1)[0]

                df = pd.read_csv(d + filename, sep=sep) #, index_col = False
        
                # m2, for observed variables, will contain the last row...
                m2 = m.copy()
                for att in observed_variables:
                    m2[att] = df[att].values[-1]

                # plus those computed variables
                m2['fmeas'] = meas.harmonic_mean([df['prec'].values[-1],df['rec'].values[-1]])
                # see note above
                m2['conv'] = df['rank'].iloc[rank_of_last_changing_value(df,'srate')]

                series += [m2]

            for m2 in series:
                for k in m2:
                    if k not in all_d:
                        all_d[k] = []
                    all_d[k] += [str(m2[k])]

    tosv = pd.DataFrame.from_dict( all_d )
    return tosv

# This was already in dfm (or stats.display.formatter)
def select(selection_function, root_dir, transpose=False, out_file=None, sep=" "):

    all_d = {}
    for d in dc.directories(root_dir):
        m = dc.read_params(d, sep)
        for filename in os.listdir(d):
            if "run" not in filename:
                continue

            df = pd.read_csv(d + filename, sep=sep)
            if transpose:
                df = df.T

            lines = selection_function(m, df)
            if lines is None:
                continue

            for m2 in lines:
                for k in m2:
                    if k not in all_d:
                        all_d[k] = []
                    all_d[k] += [str(m2[k])]

    tosv = pd.DataFrame.from_dict(all_d)
    if out_file is not None:
        tosv.to_csv(out_file, sep=sep)
    return tosv


