import pandas as pd
import re
import functools
import os

import pysake.data.measures as meas
import pysake.data.aggruns as agr 

sep = os.environ.get("sep", " ") # default to space

####### All this for legacy experiences

# Function to compare two lists
def compareLists(l1,l2):
    if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,l1,l2), True): 
        #print ("The lists l1 and l2 are the same")
        return True 
    else: 
        #print ("The lists l1 and l2 are not the same")
        return False

# Returns the prefix of the files in the results directory of the experience 'label'
# prefix could be label+nbAgents+nbIterations or just nbAgents+nbIterations
def getResultsFilePrefixes(results_directory,label,extension,nbagents,nbiterations):
    
    if label == '20170529-NOOR':
        prefixes = ['20170530-NOOR-'+nbagents+'-'+nbiterations+'-']
    else:
        prefixes = []    
        for iteration in nbiterations:
        
            prefix = label+'-'+nbagents+'-'+iteration+'-'
    
            for filename in os.listdir( results_directory ):
                if filename.endswith( extension ):
                    if not filename.startswith( prefix ):       
                        prefix = nbagents+'-'+iteration+'-'
                        break
            
            prefixes.append(prefix)
                    
    return prefixes
    
def getResultsFilePrefix(results_directory,label,extension,nbagents,nbiterations):
    
    if label == '20170529-NOOR':
        prefix = '20170530-NOOR-'+nbagents+'-'+nbiterations+'-'
    else:    
        prefix = label+'-'+nbagents+'-'+nbiterations+'-'
    
        for filename in os.listdir( results_directory ):
            if filename.endswith( extension ):
                if not filename.startswith( prefix ):       
                    prefix = nbagents+'-'+nbiterations+'-'
                    break
                    
    return prefix

# This to compute incoherence from txt file
def getIncoherenceFromFile(filenamePath):
    with open(filenamePath,"r") as file:
        totInc = 0
        n = 0
        for line in file:
            match = re.match('^(.*)Final(.*)incoherence = (.*)$', line)
            if match:
                n += 1
                totInc += float(match.group(3))
        #print(n)
        #print(totInc)
        if n>0:
            return totInc/n
        else:
            return 0

# This to detect ops and keys
def getAllKeys(results_dir,ops,postfix):

    modalities = ['syntactic','random','protected','protected-nr','clever-nr','gen','empty','str','im80','real','rr']
    modLabels = {'syntactic':'syntactic','random':'random','protected':'protected','protected-nr':'protected-nr','clever-nr':'expandAlignments','gen':'generative','empty':'startEmpty','str':'strenghen','im80':'immediateRatio','real':'realistic','rr':'rr'}
    
    allFilenames = ''
    for filename in os.listdir( results_dir ):
        if filename.endswith( postfix ):
            allFilenames += ","+filename

    allKeys = {}
    for mode in modalities:
        if allFilenames.find(mode) != -1:
            allKeys[modLabels[mode]] = []

    allOps = []
    for op in ops:
        if re.search(r'\b'+op+r'\b',allFilenames):
            allOps.append(op)
    allKeys['revisionModality'] = []

    #display(allOps)
    #display(allKeys)
    
    return allKeys

def getOpFromFile(filename,ops):

    if re.search(r'\b'+'nothing'+r'\b',filename):
        return 'nothing'
    else:
        for op in ops:
            if re.search(r'\b'+op+r'\b',filename):
                return op
            #return str(ops.index(op))+op
    return 'noop'
    
## This is maybe unuseful since all params are True !!!
def getOrderParams(keys):
    ascendingParam = []
    for key in keys:
        if key == 'ops':
            ascendingParam.append(True)
        else:
            #ascendingParam.append(False)
            ascendingParam.append(True)
    return ascendingParam

def aggregate_simple_runs(results_directory, keys, ops, observed_variables, prefix, postfix):

    modLabelsInv = {'syntactic':'syntactic','random':'random','protected':'protected','protected-nr':'protected-nr','expandAlignments':'clever-nr','generative':'gen','startEmpty':'empty','strenghen':'str','immediateRatio':'im80','realistic':'real','rr':'rr'}

    lpo = len(postfix)
    lpr = len(prefix)
    all_d = {}
    for filename in os.listdir( results_directory ):
        if filename.endswith( postfix ):

            df = pd.read_csv( results_directory+'/'+filename, sep=sep ) #, index_col = False
        
            # m, for observed variables, will contain the last row...
            m = {}

            # This for adding cols to m
            for key in keys:
                if key == 'revisionModality':
                    m[key] = getOpFromFile(filename,ops)
                elif key == 'strenghen':
                    m[key] = '-'
                    strIn = re.search(r'\b'+'str([a-z]+)'+r'\b',filename)
                    if strIn:
                        m[key] = strIn.group(0)
                else:
                    m[key] = 0
                    if filename.find(modLabelsInv[key]) != -1:
                        m[key] = 1   

            m['name'] = filename[lpr:-lpo]
            #display("filename")
            #display(filename[lpr:-lpo])
            for att in observed_variables:
                avatt = 'avg-'+att
                m[att] = df[avatt].values[-1]

            # computes derived variables
            m['fmeas'] = meas.harmonic_mean([df['avg-prec'].values[-1],df['avg-rec'].values[-1]])
        
            # get the incoherence
            if os.path.isfile(results_directory+'/'+filename[:-3]+'txt'):
                m['inc'] = getIncoherenceFromFile(results_directory+'/'+filename[:-3]+'txt')

            # get the convergence
            m['conv'] = df['rank'].iloc[agr.rank_of_last_decreasing_value(df,'avg-srate')]
            for k in m:
                if k not in all_d:
                    all_d[k] = []
                all_d[k] += [str(m[k])]
                
    return pd.DataFrame.from_dict(all_d)



