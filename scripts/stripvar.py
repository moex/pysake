# 
# This script takes a ipynb file and generates a file without variables
#
# Strip all variable content found in the context of:
#{
# "cells": [
#   {"cell_type": "markdown", 
#   "metadata": { "variables": {...to strip...}}]}
#

import json
import sys

if len(sys.argv) < 2:
  data = json.load(sys.stdin)
else:
  with open(sys.argv[1]) as data_file:
    data = json.load(data_file)

if 'cells' in data:
  for element in data['cells']:
    if 'cell_type' in element and element['cell_type'] == 'markdown' and 'metadata' in element:
      if 'variables' in element['metadata']:
#        print( "Cell: "+str(element['metadata']['variables']) )
        del element['metadata']['variables']

json.dump(data,sys.stdout,indent=1)
print()
